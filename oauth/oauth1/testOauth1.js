var https = require('https');
var assert = require('assert');
var oauth = require('./oauth1.js');
var OAuth = require('oauth');

var url = 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=2&screen_name=colmsjo';

var curlFormat = function (allParams) {
  var headers = allParams.headers,
    data = allParams.data,
    url = allParams.options.protocol + '//' + allParams.options.host +
    allParams.options.path;

  return 'curl --get \'' + url.replace(/&/g, '\\&') +
    '\' --data \'' + data + '\' ' +
    '--header \'Authorization: ' + headers + '\' --verbose';
};

// Test that the generated signature matches the twitter signature
// ===============================================================
//
// Request created with the Twitter signature generator
// [here](https://dev.twitter.com/oauth/tools/signature-generator/8523396?nid=731)

var oAuthParams = require('credentials.json').twitter0;
var oauth_signature = "N2cWhxHI6tPv0fBgxtQhqIy2WTw="; //"N2cWhxHI6tPv0fBgxtQhqIy2WTw%3D"
var expectedSignatureBaseString = "GET&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fuser_timeline.json&count%3D2%26oauth_consumer_key%3DJwO83yFv2tuY5ZKuy7Fa8H7bY%26oauth_nonce%3Dbeb6166b0e2fe5983aaf6036e6507ad1%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1436625405%26oauth_token%3D395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U%26oauth_version%3D1.0%26screen_name%3Dcolmsjo";

console.log('Nothing thrown means that everything is ok!\n');

console.log('-- Validate against old request known to be good --')

var all = oauth.sign(oAuthParams, url, 'GET');

console.log(all.signatureBaseString);
console.log(expectedSignatureBaseString)

assert(all.signatureBaseString === expectedSignatureBaseString,
  'Incorrect signature base string');

assert(all.o.oauth_signature === oauth_signature,
  'Incorrect signature');

// Print curl command for all credentials and test live requests
// =============================================================

var cred = require('credentials.json');

for (c in cred) {
  var oAuthParams = cred[c];
  var allParams = oauth.sign(oAuthParams, url, 'GET');

  console.log('\n-- result for ' + c + ':\n');
  console.log(allParams);
  console.log(curlFormat(allParams));
}
