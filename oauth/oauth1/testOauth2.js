var https = require('https');
var assert = require('assert');
var oauth1 = require('./oauth1.js');

var cred = {
  "oauth_consumer_key": "JwO83yFv2tuY5ZKuy7Fa8H7bY",
  "oauth_consumer_secret": "MZhBVEkFhon8bTFT4m87FqoqoXNJB9L22EYDOEBKXkcoKlomyS",
  "oauth_token": "395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U",
  "oauth_token_secret": "Nst5UbpqtZx9kisPO9N8LQhrHH4qaWSZRDvfgQTN0hil5",
};

var data = 'count=2&screen_name=colmsjo';
var url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

var performHttpReq = function (options, data) {

  var req = https.request(options, function (res) {
    var str = '';

    res.on('data', function (chunk) {
      str += chunk;
    });

    res.on('end', function () {
      req.end();
      console.log('\n-- Request using standard https library with auth headers --');
      console.log(str);
    });
  });

  req.write(data);
};

var allParams = oauth1.sign(cred, url, 'GET');
performHttpReq(allParams.options, data);
