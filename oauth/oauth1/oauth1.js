// Tool for generating oauth1 signatures
// Jonas Colmsjö, 150712
// MIT LICENSE
//
// Based on [OAuthSimple](https://github.com/jrconlin/oauthsimple) but most
// parts are re-written from scratch in irder to make it easy to verify
// the implementation.

var URL = require('url');
var querystring = require('querystring');

// # Helpers

var base64HmacSha1 = function (secretKey, sigString, b64pad, characterSize) {

  var k = secretKey,
    d = sigString,
    _p = b64pad,
    _z = characterSize;

  // heavily optimized and compressed version of http://pajhome.org.uk/crypt/md5/sha1.js
  // where _p = b64pad, _z = character size; not used here but I left them available just in case
  if (!_p) {
    _p = '=';
  }
  if (!_z) {
    _z = 8;
  }

  function _f(t, b, c, d) {
    if (t < 20) {
      return (b & c) | ((~b) & d);
    }
    if (t < 40) {
      return b ^ c ^ d;
    }
    if (t < 60) {
      return (b & c) | (b & d) | (c & d);
    }
    return b ^ c ^ d;
  }

  function _k(t) {
    return (t < 20) ? 1518500249 : (t < 40) ? 1859775393 : (t < 60) ? -1894007588 : -899497514;
  }

  function _s(x, y) {
    var l = (x & 0xFFFF) + (y & 0xFFFF),
      m = (x >> 16) + (y >> 16) + (l >> 16);
    return (m << 16) | (l & 0xFFFF);
  }

  function _r(n, c) {
    return (n << c) | (n >>> (32 - c));
  }

  function _c(x, l) {
    x[l >> 5] |= 0x80 << (24 - l % 32);
    x[((l + 64 >> 9) << 4) + 15] = l;
    var w = [80],
      a = 1732584193,
      b = -271733879,
      c = -1732584194,
      d = 271733878,
      e = -1009589776;
    for (var i = 0; i < x.length; i += 16) {
      var o = a,
        p = b,
        q = c,
        r = d,
        s = e;
      for (var j = 0; j < 80; j++) {
        if (j < 16) {
          w[j] = x[i + j];
        } else {
          w[j] = _r(w[j - 3] ^ w[j - 8] ^ w[j - 14] ^ w[j - 16], 1);
        }
        var t = _s(_s(_r(a, 5), _f(j, b, c, d)), _s(_s(e, w[j]), _k(j)));
        e = d;
        d = c;
        c = _r(b, 30);
        b = a;
        a = t;
      }
      a = _s(a, o);
      b = _s(b, p);
      c = _s(c, q);
      d = _s(d, r);
      e = _s(e, s);
    }
    return [a, b, c, d, e];
  }

  function _b(s) {
    var b = [],
      m = (1 << _z) - 1;
    for (var i = 0; i < s.length * _z; i += _z) {
      b[i >> 5] |= (s.charCodeAt(i / 8) & m) << (32 - _z - i % 32);
    }
    return b;
  }

  function _h(k, d) {
    var b = _b(k);
    if (b.length > 16) {
      b = _c(b, k.length * _z);
    }
    var p = [16],
      o = [16];
    for (var i = 0; i < 16; i++) {
      p[i] = b[i] ^ 0x36363636;
      o[i] = b[i] ^ 0x5C5C5C5C;
    }
    var h = _c(p.concat(_b(d)), 512 + d.length * _z);
    return _c(o.concat(h), 512 + 160);
  }

  function _n(b) {
    var t = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
      s = '';
    for (var i = 0; i < b.length * 4; i += 3) {
      var r = (((b[i >> 2] >> 8 * (3 - i % 4)) & 0xFF) << 16) | (((b[i + 1 >> 2] >> 8 * (3 - (i + 1) % 4)) & 0xFF) << 8) | ((b[i + 2 >> 2] >> 8 * (3 - (i + 2) % 4)) & 0xFF);
      for (var j = 0; j < 4; j++) {
        if (i * 8 + j * 6 > b.length * 32) {
          s += _p;
        } else {
          s += t.charAt((r >> 6 * (3 - j)) & 0x3F);
        }
      }
    }
    return s;
  }

  function _x(k, d) {
    return _n(_h(k, d));
  }
  return _x(k, d);
};

var escape = function (string) {
  if (string === undefined) {
    return '';
  }
  if (string instanceof Array) {
    throw ('Array passed to _oauthEscape');
  }
  return encodeURIComponent(string).replace(/\!/g, '%21').
  replace(/\*/g, '%2A').
  replace(/'/g, '%27').
  replace(/\(/g, '%28').
  replace(/\)/g, '%29');
};

var nonce = function (length) {
  var nonce_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

  if (length === undefined) {
    length = 32;
  }
  var result = '',
    i = 0,
    rnum,
    cLength = nonce_chars.length;
  for (; i < length; i++) {
    rnum = Math.floor(Math.random() * cLength);
    result += nonce_chars.substring(rnum, rnum + 1);
  }
  return result;
};

var normalizeParameters = function (params) {
  var elements = [],
    paramNames = [],
    i = 0,
    ra = 0;

  for (var paramName in params) {
    if (params.hasOwnProperty(paramName)) {
      if (ra++ > 1000) {
        throw ('runaway 1');
      }
      paramNames.unshift(paramName);
    }
  }

  paramNames = paramNames.sort();
  var pLen = paramNames.length;
  for (; i < pLen; i++) {
    paramName = paramNames[i];
    //skip secrets.
    if (paramName.match(/\w+_secret/)) {
      continue;
    }
    if (params[paramName] instanceof Array) {
      var sorted = params[paramName].sort(),
        spLen = sorted.length,
        j = 0;
      for (; j < spLen; j++) {
        if (ra++ > 1000) {
          throw ('runaway 1');
        }
        elements.push(escape(paramName) + '=' +
          escape(sorted[j]));
      }
      continue;
    }
    elements.push(escape(paramName) + '=' +
      escape(params[paramName]));
  }

  return elements.join('&');
};

merge = function (source, target) {
  if (source === undefined)
    source = {};
  if (target === undefined)
    target = {};
  for (var key in source) {
    if (source.hasOwnProperty(key)) {
      target[key] = source[key];
    }
  }
  return target;
};

timestamp = function () {
  return Math.floor((new Date()).getTime() / 1000);
};

var defaults = function (o) {
  o.oauth_nonce = o.oauth_nonce || nonce();
  o.oauth_version = o.oauth_version || "1.0";
  o.oauth_timestamp = o.oauth_timestamp || timestamp();
  o.oauth_signature_method = o.oauth_signature_method || 'HMAC-SHA1';

  return o;
};

// # Signing function

// ## Example from twitters OAuth signing tool that is used for testing
//
// OAuth Signing Results
// Important: This will only be valid for a few minutes. Also remember the cURL
// command will actually execute the request.
//
// Signature base string:
//
//```
// GET&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fuser_timeline.json&
// count%3D2%26
// oauth_consumer_key%3DJwO83yFv2tuY5ZKuy7Fa8H7bY%26
// oauth_nonce%3Dbeb6166b0e2fe5983aaf6036e6507ad1%26
// oauth_signature_method%3DHMAC-SHA1%26
// oauth_timestamp%3D1436625405%26
// oauth_token%3D395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U%26
// oauth_version%3D1.0%26
// screen_name%3Dcolmsjo
//```
//
// Authorization header:
//
//```
// Authorization: OAuth oauth_consumer_key="JwO83yFv2tuY5ZKuy7Fa8H7bY",
// oauth_nonce="beb6166b0e2fe5983aaf6036e6507ad1",
// oauth_signature="N2cWhxHI6tPv0fBgxtQhqIy2WTw%3D",
// oauth_signature_method="HMAC-SHA1",
// oauth_timestamp="1436625405",
// oauth_token="395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U",
// oauth_version="1.0"
//```
//
// cURL command:
//
//```
// curl --get 'https://api.twitter.com/1.1/statuses/user_timeline.json' \
// --data 'count=2&screen_name=colmsjo' \
// --header 'Authorization: OAuth oauth_consumer_key="JwO83yFv2tuY5ZKuy7Fa8H7bY", \
// oauth_nonce="beb6166b0e2fe5983aaf6036e6507ad1", \
// oauth_signature="N2cWhxHI6tPv0fBgxtQhqIy2WTw%3D", \
// oauth_signature_method="HMAC-SHA1", oauth_timestamp="1436625405", \
// oauth_token="395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U", \
// oauth_version="1.0"' --verbose
//```

var oauth = {};

var signatureBaseString = function (o, data, options) {

  if (o.oauth_signature_method !== 'HMAC-SHA1' &&
    o.oauth_signature_method !== 'PLAINTEXT') {

    throw 'Signature method not suported: ' + o.oauth_signature_method;
  }

  if (o.oauth_signature_method === 'PLAINTEXT') {
    return this.secretKey(o);
  }

  var params = merge(data, {});
  params = merge(o, params);

  var normParams = normalizeParameters(params);

  return escape(options.method) + '&' +
    escape(options.protocol + '//' + options.host + options.path) + '&' +
    escape(normParams);
};

var secretKey = function (oAuthParams) {
  return escape(oAuthParams.oauth_consumer_secret) + '&' +
    escape(oAuthParams.oauth_token_secret);
};

var headers = function (o) {

  if (!o.oauth_signature) {
    console.log(o);
    throw "Internal error, missing signature";
  }

  var j, pName, pLength, result = 'OAuth ';
  var pNames = Object.keys(o).sort();

  pNames.forEach(function (pName) {
    if (o.hasOwnProperty(pName)) {
      if (pName.match(/secret/)) {
        return;
      }
      if ((o[pName]) instanceof Array) {
        pLength = o[pName].length;
        for (j = 0; j < pLength; j++) {
          result += pName + '="' + escape(o[pName][j]) + '",';
        }
      } else {
        result += pName + '="' + escape(o[pName]) + '",';
      }
    }
  });

  return result.replace(/,\s+$/, '');
};

oauth.sign = function (oAuthParams, url, method) {
  var a = {},
    o = defaults(merge(oAuthParams, {}));

  if (!o.oauth_consumer_secret || !o.oauth_consumer_key || !url || !method
//    || !o.oauth_token_secret || !o.oauth_token
  ) {

    throw 'Mandatory parameter missing';
  }

  var url = URL.parse(url);

  a.options = {};
  a.options.method = method;
  a.options.protocol = url.protocol
  a.options.port = (url.protocol === 'http:') ? 80 : 443;
  a.options.host = url.host;
  a.options.path = url.pathname;
  a.options.headers = {
    Host: url.host,
    Accept: '*/*',
    Connection: 'close',
    'User-Agent': 'Node authentication',
    'Content-length': 0,
    'Content-Type': 'application/x-www-form-urlencoded'
  }

  a.data = querystring.parse(url.query);

  a.o = o;

  a.secretKey = secretKey(o);
  a.signatureBaseString = signatureBaseString(o, a.data, a.options);
  o.oauth_signature = base64HmacSha1(a.secretKey,
    a.signatureBaseString);

  a.data = normalizeParameters(a.data);
  a.headers = headers(o, a.options);
  a.headers = a.headers.slice(0,-1);

  a.options.headers.Authorization = headers(o, a.options);

  return a;
};

module.exports = oauth;
