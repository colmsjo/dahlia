OAuth notes
==========

node-oauth
----------

OAuth library for NodeJS. Ok but won't work in browsers since the HTTPRequest
is performed from within the library. It would be better if the library only
prepared the headers and SHA hash.

* [npmjs docs](https://www.npmjs.com/package/oauth)
* [example of use](./node-oauth)


oauthsimple by JR Conlin
-------------------------

This library works in the browser but it did not 'work out of the box'. It is
also difficult to debug what's happening.

 * [github repo](https://github.com/jrconlin/oauthsimple)
 * [example of use](./oauthsimple)


new library based on oauthsimple
--------------------------------

I've based a new library on oauthsimple. The key function is `base64HmacSha1`
which Conlin had taken from Paul Johnston. This library also returns intermediate
results (secret key, headers, signature base string). The HTTP request is
performed by the user of the library so it should work in any JavaScript
environment (NodeJS, browsers, etc.).

I've tested this library with Twitter and it works fine so far.


More libraries
-------------

 * https://www.npmjs.com/package/grant - tested and I did not like it
