Examples of usage
=================

Base URL (gives 404): `curl -v https://dahlia.gizur.com/magento/api/rest`

Magento uses three legged oauth 1.0a, see
http://oauthbible.com/#oauth-10a-three-legged for an introduction.

Twitter also supports three legged oauth, see
https://dev.twitter.com/oauth/3-legged

Authentication
=============

1. In admin: `System->Web services->REST - OAuth consumers->Add new`

 * Name: `jonas`
 * Example key: `4b84d84a74728b75b1d3738878a89f14`
 * Example secret: `011e0721a828812057e6d8fe085c47da`

2. Get request token: `curl -v -X POST https://dahlia.gizur.com/magento/oauth/initiate`


Resources
==========

Links:

 * http://www.magentocommerce.com/api/rest/introduction.html
 * http://oauth.net/code/
