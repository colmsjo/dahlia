var https = require('https');
var assert = require('assert');
var OAuth = require('oauth');

var cred = {
  "oauth_consumer_key": "JwO83yFv2tuY5ZKuy7Fa8H7bY",
  "oauth_consumer_secret": "MZhBVEkFhon8bTFT4m87FqoqoXNJB9L22EYDOEBKXkcoKlomyS",
  "oauth_token": "395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U",
  "oauth_token_secret": "Nst5UbpqtZx9kisPO9N8LQhrHH4qaWSZRDvfgQTN0hil5",
};

var oauth = new OAuth.OAuth(
  'https://api.twitter.com/oauth/request_token',
  'https://api.twitter.com/oauth/access_token',
  cred.oauth_consumer_key,
  cred.oauth_consumer_secret,
  '1.0A',
  null,
  'HMAC-SHA1'
);

var data = 'count=2&screen_name=colmsjo';

var options = {
  host: 'api.twitter.com',
  port: 443,
  path: '/1.1/statuses/user_timeline.json?',
  method: 'GET',
  headers: {
    Host: 'api.twitter.com',
    Accept: '*/*',
    Connection: 'close',
    'User-Agent': 'Node authentication',
    'Content-length': 0,
    'Content-Type': 'application/x-www-form-urlencoded'
  }
};

var url = 'https://' + options.host + options.path;

var performHttpReq = function (options, data, authHeader) {

  var req = https.request(options, function (res) {
    var str = '';

    res.on('data', function (chunk) {
      str += chunk;
    });

    res.on('end', function () {
      req.end();
      console.log('\n-- Request using standard https library with auth headers --');
      console.log(options);
      console.log(str);
    });
  });

  req.write(data);
};

var toAuthObj = function (authHeader) {
  var a = authHeader.
    substr(6, authHeader.length).
    replace(/\"/g, '').
    split(',').
    map(function (e) { return e.split('=') });

  return a.reduce(function (prev, curr, idx, arr) {
    prev[curr[0]] = curr[1];
    return prev;
  }, {});
};

var authHeader = oauth.authHeader(url, cred.oauth_token,
  cred.oauth_token_secret);

console.log(toAuthObj(authHeader));

oauth.get(
  url,
  cred.oauth_token,
  cred.oauth_token_secret,
  function (e, data, res) {
    if (e) console.error(e);
    console.log('\n-- Request using node-oauth library --')
    console.log(options);
    console.log(require('util').inspect(data));
  });

options.headers['Authorization'] = authHeader;
performHttpReq(options, data, authHeader);
