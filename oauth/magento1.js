var https = require('https');
var assert = require('assert');
var oauth1 = require('./oauth1/oauth1.js');

var cred = {
  "oauth_consumer_key": "4b84d84a74728b75b1d3738878a89f14",
  "oauth_consumer_secret": "011e0721a828812057e6d8fe085c47da"
};

var data = '';
var callbackUrl = 'http://localhost';
var url1 = 'https://dahlia.gizur.com/magento/oauth/initiate?oauth_callback=' + callbackUrl;

var performHttpReq = function (options, data) {

  var req = https.request(options, function (res) {
    var str = '';

    res.on('data', function (chunk) {
      str += chunk;
    });

    res.on('end', function () {
      req.end();
      console.log('\n-- Request using standard https library with auth headers --');
      console.log(str);
    });
  });

  req.write(data);
};

var allParams = oauth1.sign(cred, url1, 'GET');
allParams.options.rejectUnauthorized = false;
console.log(allParams);
performHttpReq(allParams.options, data);
