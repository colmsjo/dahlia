#!/usr/bin/env node
var OAuthSimple = require('./OAuthSimple.js');

var curlFormat = function(url, header) {
  return 'curl -v --get ' +
         '--header \'Authorization: ' + header + '\' ' +
          url.replace(/&/g,'\\&');
}

var curlFormat2 = function(url, header, data) {
  return 'curl -v --get ' +
         '--header \'Authorization: ' + header + '\' ' +
         '--data \'' + data + '\' ' +
          url.replace(/&/g,'\\&');
}

var apiKey = "JwO83yFv2tuY5ZKuy7Fa8H7bY"; // consumer key
var sharedSecret = "MZhBVEkFhon8bTFT4m87FqoqoXNJB9L22EYDOEBKXkcoKlomyS"; // consumer secret
var accessToken = "395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U";
var tokenSecret = "Nst5UbpqtZx9kisPO9N8LQhrHH4qaWSZRDvfgQTN0hil5";

var path = "https://api.twitter.com/1.1/statuses/user_timeline.json";
var argumentsAsString = "count=2&screen_name=twitterapi";
var argumentsAsObject = {
  count: 2,
  screen_name: 'twitterapi'
};

// Test 1 ===== The hard way...
var oauth = OAuthSimple(apiKey, sharedSecret);
oauth.setParameters(argumentsAsString);
oauth.setPath(path);
var sample1Results = oauth.sign();
//console.log('-- Sample 1 --\n', sample1Results);
console.log('curl format:', curlFormat(sample1Results.signed_url, sample1Results.header));
console.log('\ncurl format:', curlFormat2(path, sample1Results.header, argumentsAsString));

// Test 2 ===== the semi-easy way.
OAuthSimple().reset();
var oauth2 = OAuthSimple(apiKey, sharedSecret);
var sample2Results = oauth2.sign({
  action: 'GET',
  path: path,
  method: 'HMAC-SHA1',
  parameters: argumentsAsObject
});
//console.log('\n-- Sample 2 --\n', sample2Results);
console.log('\n\ncurl format:', curlFormat(sample2Results.signed_url, sample2Results.header));
console.log('\ncurl format:', curlFormat2(path, sample2Results.header, argumentsAsString));

// Test 3 ===== the totally easy way.
OAuthSimple().reset();
var sample3Results = (new OAuthSimple()).sign({
  path: path,
  parameters: argumentsAsString,
  signatures: {
    'consumer_key': apiKey,
    'shared_secret': sharedSecret,
    'access_token': accessToken,
    'access_secret': tokenSecret
  }
});
//console.log('\n-- Sample 3 --\n', sample3Results);
console.log('\n\ncurl format:', curlFormat(sample3Results.signed_url, sample3Results.header));
console.log('\ncurl format:', curlFormat2(path, sample3Results.header, argumentsAsString));
