Twitter OAuth 1.0a notes
========================

Intro
-----

Twitter provides a tool that generates signatures for oauth requests. This is usefull when
developing, see an example 
[here](https://dev.twitter.com/oauth/tools/signature-generator/8523396?nid=731)

First, create a twitter [app](http://apps.twitter.com)

[`json`] pretty prints json on the command line. Install with: `npm install -g json` 


Examples
--------

[API](https://dev.twitter.com/rest/reference/get/statuses/user_timeline)

```
curl --get 'https://api.twitter.com/1.1/statuses/user_timeline.json' \
--data 'count=2&screen_name=twitterapi' \
--header 'Authorization: OAuth oauth_consumer_key="JwO83yFv2tuY5ZKuy7Fa8H7bY", \
oauth_nonce="8c6d183fa3226bf6922e4bada6dc3b1c", \
oauth_signature="K5xzW6EFHtydxW8%2BpqLg%2BpdqJlw%3D", \
oauth_signature_method="HMAC-SHA1", oauth_timestamp="1436624685", \
oauth_token="395234364-b0z7YRGtLQicATtx1k0PdRSWZI8Bd0wRJXq4oL8U", oauth_version="1.0"' --verbose \
| json
```


Links
-----

 * https://dev.twitter.com/rest/public

