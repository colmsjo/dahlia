Selection of CSS library
=======================


Alternatives:

 * Bootstrap
 * Foundation
 * A bunch of lightweight alternatives, more below

One difference is that Bootstrap uses pixels and Foundation rems.

Lightweight alternatives:

 * [960gs](http://960.gs) - very common, provides a grid only
 * [unsemantic](http://unsemantic.com) - responsive successor to 960gs
 * [Frameless](http://framelessgrid.com) - responsive, non-fluid grid
 * [skeleton](http://getskeleton.com) - seams to be the most common alternative to 
Boostrap and Foundation
 * [Pure](http://purecss.io) - from Yahoo
 * [Less](http://lessframework.com)
 * [app.js](http://code.kik.com/app/2/index.html)

NOTE: flexbox is a new standard CSS construct that should help developing
responsive apps. It is only supported in newer browser versions
though.


Links:

 * [Syntax of bootstrap vs foundation](http://responsive.vermilion.com/compare.php)
 * [lightweight bootstrap alternatives](http://www.hongkiat.com/blog/bootstrap-alternatives/)
 * [bootstrap vs foundation](http://blog.teamtreehouse.com/use-bootstrap-or-foundation)
 * [CSS Frameworks that aren't foundation or bootstrap](http://snip.ly/hlcX#http://feedproxy.google.com/~r/boogiesbc/~3/1_r9USRjTME/)
 * [Post that include Golden Grid, LESS etc.](http://line25.com/articles/which-responsive-frameworks-are-designers-using)


Hybrid frameworks:

 * ionic - on top of angular
 * onsen - ionic competitor, also on angular


HTML UI Libraries
---------------------

Some new alternatives

 * [Semantic-UI](http://semantic-ui.com)
 * Brick
 * UIKit


Links:

 * http://www.sitepoint.com/beyond-bootstrap-foundation-frameworks-never-heard/


Grids
-----

 * 960gs is very commonly used
 * Canonical grids are used in Java

[Gridstylesheets](http://gridstylesheets.org) is based  Cassowary Constraint Solver, the same 
algorithm Apple uses to compute native layout. Note everyone likes it though, see 
[this y-combinator post](https://news.ycombinator.com/item?id=9677306)




Notes
------

Angular is JS frontend library. Alternatives
 * react - Facebook
 * ember - 
 * blaze - Meteor

Meteror is a full stack built on NodeJS
